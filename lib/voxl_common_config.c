/*******************************************************************************
 * Copyright 2023 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <math.h>

#include <modal_json.h>
#include "voxl_common_config.h"

//#define DEBUG


#define EXTRINSIC_CONFIG_FILE_HEADER "\
/**\n\
 * Extrinsic Configuration File\n\
 * This file is used by voxl-vision-hub, voxl-qvio-server, and voxl-openvins-server\n\
 *\n\
 * This configuration file serves to describe the static relations (translation\n\
 * and rotation) between sensors and bodies on a drone. Mostly importantly it\n\
 * configures the camera-IMU extrinsic relation for use by VIO. However, the\n\
 * user may expand this file to store many more relations if they wish. By\n\
 * consolidating these relations in one file, multiple processes that need this\n\
 * data can all be configured by this one configuration file. Also, copies of\n\
 * this file may be saved which describe particular drone platforms. The\n\
 * defaults describe the VOXL M500 drone reference platform.\n\
 *\n\
 * The file is constructed as an array of multiple extrinsic entries, each\n\
 * describing the relation from one parent to one child. Nothing stops you from\n\
 * having duplicates but this is not advised.\n\
 *\n\
 * The rotation is stored in the file as a Tait-Bryan rotation sequence in\n\
 * intrinsic XYZ order in units of degrees. This corresponds to the parent\n\
 * rolling about its X axis, followed by pitching about its new Y axis, and\n\
 * finally yawing around its new Z axis to end up aligned with the child\n\
 * coordinate frame.\n\
 *\n\
 * The helper read function will read out and populate the associated data\n\
 * struct in both Tait-Bryan and rotation matrix format so the calling process\n\
 * can use either. Helper functions are provided to convert back and forth\n\
 * between the two rotation formats.\n\
 *\n\
 * Note that we elect to use the intrinsic XYZ rotation in units of degrees for\n\
 * ease of use when doing camera-IMU extrinsic relations in the field. This is\n\
 * not the same order as the aerospace yaw-pitch-roll (ZYX) sequence as used by\n\
 * the rc_math library. However, since the camera Z axis points out the lens, it\n\
 * is helpful for the last step in the rotation sequence to rotate the camera\n\
 * about its lens after first rotating the IMU's coordinate frame to point in\n\
 * the right direction by Roll and Pitch.\n\
 *\n\
 * The following online rotation calculator is useful for experimenting with\n\
 * rotation sequences: https://www.andre-gaschler.com/rotationconverter/\n\
 *\n\
 * The Translation vector should represent the center of the child coordinate\n\
 * frame with respect to the parent coordinate frame in units of meters.\n\
 *\n\
 * The parent and child name strings should not be longer than 63 characters.\n\
 *\n\
 * The relation from Body to Ground is a special case where only the Z value is\n\
 * read by voxl-vision-px4 and voxl-qvio-server so that these services know the\n\
 * height of the drone's center of mass (and tracking camera) above the ground\n\
 * when the drone is sitting on its landing gear ready for takeoff.\n\
 *\n\
 **/\n"




void vcc_print_extrinsic_conf(vcc_extrinsic_t* t, int n)
{
	int i,j;

	for(i=0; i<n; i++){
		printf("#%d:\n",i);
		printf("    parent:                %s\n", t[i].parent);
		printf("    child:                 %s\n", t[i].child);
		printf("    T_child_wrt_parent:  ");
		for(j=0;j<3;j++) printf("%7.3f ",t[i].T_child_wrt_parent[j]);
		printf("\n    RPY_parent_to_child:");
		for(j=0;j<3;j++) printf("%6.1f  ",t[i].RPY_parent_to_child[j]);
		printf("\n    R_child_to_parent:   ");
		for(j=0;j<3;j++) printf("%7.3f ", t[i].R_child_to_parent[0][j]);
		printf("\n                         ");
		for(j=0;j<3;j++) printf("%7.3f ", t[i].R_child_to_parent[1][j]);
		printf("\n                         ");
		for(j=0;j<3;j++) printf("%7.3f ", t[i].R_child_to_parent[2][j]);
		printf("\n");
	}
	return;
}


// this would be much shorter lib librc_math, but we hand write the math to
// avoid having a dependency
static int _invert_tf(vcc_extrinsic_t in, vcc_extrinsic_t* out)
{

	if(out==NULL){
		fprintf(stderr, "ERROR in %s, received NULL pointer\n", __FUNCTION__);
		return -1;
	}

	// copy over parent/child in reverse
	strcpy(out->child, in.parent);
	strcpy(out->parent, in.child);

	// rotation matrix is tranpose
	out->R_child_to_parent[0][0] = in.R_child_to_parent[0][0];
	out->R_child_to_parent[0][1] = in.R_child_to_parent[1][0];
	out->R_child_to_parent[0][2] = in.R_child_to_parent[2][0];
	out->R_child_to_parent[1][0] = in.R_child_to_parent[0][1];
	out->R_child_to_parent[1][1] = in.R_child_to_parent[1][1];
	out->R_child_to_parent[1][2] = in.R_child_to_parent[2][1];
	out->R_child_to_parent[2][0] = in.R_child_to_parent[0][2];
	out->R_child_to_parent[2][1] = in.R_child_to_parent[1][2];
	out->R_child_to_parent[2][2] = in.R_child_to_parent[2][2];

	// multiply new rotation matrix by negative of old translation to get new translation
	for(int i=0; i<3; i++){
		out->T_child_wrt_parent[i] = 0.0;
		for(int j=0; j<3; j++){
			out->T_child_wrt_parent[i] -= out->R_child_to_parent[i][j] * in.T_child_wrt_parent[j];
		}
	}

	// populate tait-bryan angles too
	vcc_rotation_matrix_to_tait_bryan_xyz_degrees(out->R_child_to_parent, out->RPY_parent_to_child);

	return 0;
}

// here A is the grandparent, B is parent, C is child
static int _combine_tf(vcc_extrinsic_t AB, vcc_extrinsic_t BC, vcc_extrinsic_t* AC)
{
	int i,j,k;

	//sanity checks
	if(AC==NULL){
		fprintf(stderr, "ERROR in %s, received NULL pointer\n", __FUNCTION__);
		return -1;
	}

	memset(AC, 0, sizeof(vcc_extrinsic_t));
	strcpy(AC->parent, AB.parent);
	strcpy(AC->child, BC.child);

	// combined rotation matrix
	for(i=0;i<3;i++){
		for(j=0;j<3;j++){
			for(k=0;k<3;k++){
				AC->R_child_to_parent[i][j] += AB.R_child_to_parent[i][k] * BC.R_child_to_parent[k][j];
			}
		}
	}

	// translation
	for(i=0;i<3;i++){
		AC->T_child_wrt_parent[i] = AB.T_child_wrt_parent[i];
		for(j=0;j<3;j++){
			AC->T_child_wrt_parent[i] += AB.R_child_to_parent[i][j] * BC.T_child_wrt_parent[j];
		}
	}

	// populate tait-bryan angles too
	vcc_rotation_matrix_to_tait_bryan_xyz_degrees(AC->R_child_to_parent, AC->RPY_parent_to_child);
	return 0;
}


int vcc_read_extrinsic_conf_file(const char* path, vcc_extrinsic_t* t, int* n, int max_t)
{
	// vars and defaults
	int i, m;

	//sanity checks
	if(t==NULL || n==NULL){
		fprintf(stderr, "ERROR in %s, received NULL pointer\n", __FUNCTION__);
		return -1;
	}
	if(max_t<2){
		fprintf(stderr, "ERROR in %s, maximum number of extrinsics must be >=2\n", __FUNCTION__);
		return -1;
	}

	// read the data in
	cJSON* parent = json_read_file(path);
	if(parent==NULL){
		fprintf(stderr, "ERROR in %s, missing extrinsics file %s\n", __FUNCTION__, path);
		fprintf(stderr, "Likely you need to run voxl-configure-extrinsics to make a new file\n");
		return -1;
	}

	// file structure is just one big array of config structures
	cJSON* array = json_fetch_array_and_add_if_missing(parent, "extrinsics", &m);
	if(m > max_t){
		fprintf(stderr, "ERROR found %d extrinsics in file but maximum number of tags is set to %d\n", m, max_t);
		return -1;
	}

	// Now actually parse the json data into our output array!!
	for(i=0; i<m; i++){
		cJSON* item = cJSON_GetArrayItem(array, i);
		json_fetch_string(		item, "parent",			t[i].parent, 64);
		json_fetch_string(		item, "child",			t[i].child,  64);
		json_fetch_fixed_vector(item,"T_child_wrt_parent",t[i].T_child_wrt_parent, 3);
		json_fetch_fixed_vector(item,"RPY_parent_to_child",t[i].RPY_parent_to_child, 3);
		// also write out the rotation matrix equivalent
		vcc_tait_bryan_xyz_degrees_to_rotation_matrix(t[i].RPY_parent_to_child, t[i].R_child_to_parent);
	}

	// check if we got any errors in that process
	if(json_get_parse_error_flag()){
		fprintf(stderr, "failed to parse data in %s\n", path);
		cJSON_Delete(parent);
		return -1;
	}

	// write modified data to disk if neccessary
	if(json_get_modified_flag()){
		printf("The JSON extrinsic data was modified during parsing, saving the changes to disk\n");
		json_write_to_file_with_header(path, parent, EXTRINSIC_CONFIG_FILE_HEADER);
	}
	cJSON_Delete(parent);
	*n = m;
	return 0;
}


int vcc_find_extrinsic_in_array(const char* parent, const char* child, vcc_extrinsic_t* t, int n, vcc_extrinsic_t* out)
{
	if(out==NULL){
		fprintf(stderr, "ERROR in %s, received NULL pointer\n", __FUNCTION__);
		return -1;
	}

	// start at beggining of array and check untill we find a match
	for(int i=0;i<n;i++){
		// check if parent and child match
		if(strcmp(t[i].parent, parent)==0 && strcmp(t[i].child, child)==0){
			// we are done, copy and return
			memcpy(out, &t[i], sizeof(vcc_extrinsic_t));
			return 0;
		}
	}
	// rescan for inverse. Yes yes, I know this isn't optimum. We will do a
	// proper graph data structure for this soon.
	for(int i=0;i<n;i++){
		// check if parent and child match backwards and return the inverse
		if(strcmp(t[i].parent, child)==0 && strcmp(t[i].child, parent)==0){
			return _invert_tf(t[i], out);
		}
	}
	// no match, return failure
	return -1;
}


int vcc_find_extrinsic(const char* parent, const char* child, vcc_extrinsic_t* t, int n, vcc_extrinsic_t* out)
{
	// first, if we can find the correct transform already in the list
	// just use that.
	if(vcc_find_extrinsic_in_array(parent, child, t, n, out)==0){
		#ifdef DEBUG
		printf("DEBUG: Found direct map from %s to %s\n", parent, child);
		#endif
		return 0;
	}

	vcc_extrinsic_t i_to_child;
	char* intermediary = NULL;

	// otherwise scan through for an intermediary
	for(int i=0;i<n;i++){
		// check if parent and child match
		if(strcmp(t[i].parent, parent)==0){
			intermediary = t[i].child;
			if(vcc_find_extrinsic_in_array(intermediary, child, t, n, &i_to_child)==0){
				// success
				#ifdef DEBUG
				printf("DEBUG: forward map from %s to %s using %s as intermediary\n", parent, child, intermediary);
				#endif
				return _combine_tf(t[i], i_to_child, out);
			}
		}
	}

	// also check backwards
	for(int i=0;i<n;i++){
		// check if parent and child match
		if(strcmp(t[i].child, parent)==0){
			intermediary = t[i].parent;
			if(vcc_find_extrinsic_in_array(intermediary, child, t, n, &i_to_child)==0){
				// success
				#ifdef DEBUG
				printf("DEBUG: backward map from %s to %s using %s as intermediary\n", parent, child, intermediary);
				#endif
				vcc_extrinsic_t parent_to_i;
				_invert_tf(t[i], &parent_to_i);
				return _combine_tf(parent_to_i, i_to_child, out);
			}
		}
	}

	#ifdef DEBUG
	printf("DEBUG: Failed to find map from %s to %s\n", parent, child);
	#endif

	return -1;
}


void vcc_rotation_matrix_to_tait_bryan_xyz_degrees(double R[3][3], double tb[3])
{
	#ifndef RAD_TO_DEG
	#define RAD_TO_DEG (180.0/M_PI)
	#endif

	tb[1] = asin(R[0][2])*RAD_TO_DEG;
	if(fabs(R[0][2])<0.9999999){
		tb[0] = atan2(-R[1][2], R[2][2])*RAD_TO_DEG;
		tb[2] = atan2(-R[0][1], R[0][0])*RAD_TO_DEG;
	}
	else{
		tb[0] = atan2(-R[2][1], R[1][1])*RAD_TO_DEG;
		tb[2] = 0.0;
	}
	return;
}

void vcc_tait_bryan_xyz_degrees_to_rotation_matrix(double tb[3], double R[3][3])
{
	#ifndef DEG_TO_RAD
	#define DEG_TO_RAD (M_PI/180.0)
	#endif

	const double cx = cos(tb[0]*DEG_TO_RAD);
	const double sx = sin(tb[0]*DEG_TO_RAD);
	const double cy = cos(tb[1]*DEG_TO_RAD);
	const double sy = sin(tb[1]*DEG_TO_RAD);
	const double cz = cos(tb[2]*DEG_TO_RAD);
	const double sz = sin(tb[2]*DEG_TO_RAD);
	const double cxcz = cx * cz;
	const double cxsz = cx * sz;
	const double sxcz = sx * cz;
	const double sxsz = sx * sz;

	R[0][0] =  cy * cz;
	R[0][1] = -cy * sz;
	R[0][2] =  sy;
	R[1][0] =  cxsz + sxcz * sy;
	R[1][1] =  cxcz - sxsz * sy;
	R[1][2] = -sx * cy;
	R[2][0] =  sxsz - cxcz * sy;
	R[2][1] =  sxcz + cxsz * sy;
	R[2][2] =  cx * cy;

	return;
}


