/*******************************************************************************
 * Copyright 2020 - 2023 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <pthread.h>
#include <stdio.h>
#include <signal.h>
#include <getopt.h>
#include <time.h>
#include <unistd.h> // for usleep()
#include <string.h>
#include <stdlib.h> // for atoi()
#include <math.h>

#include <modal_start_stop.h>
#include <modal_pipe_client.h>

#include "common.h"

#define max(x, y) (((x) > (y)) ? (x) : (y))
#define gotoxy(x,y) printf("\033[%d;%dH", ((y)+1), ((x)+1));
typedef enum { false, true } bool;


#define CLIENT_NAME     "voxl-inspect-cam"
#define RINGBUF_SIZE    60
#define RINGBUF_WAKEUP  5 // number of frames to wait before doing fps/mbps calc
#define MAX_CAMS        PIPE_CLIENT_MAX_CHANNELS

static pthread_t parser_thread;
static pthread_mutex_t mutex;
static char pipe_path[MAX_CAMS][MODAL_PIPE_MAX_PATH_LEN];
static int  max_name_length = 9; //Length of string: "Pipe Name", can get longer if needed
static int  num_pipes = 0;
static bool use_all = false;
static bool test_mode = false;
static int en_newline = 0;
static int test_passed = 0;

// ring buffer of data for calculating fps and bitrate
static uint64_t times[MAX_CAMS][RINGBUF_SIZE];
static uint64_t latencies[MAX_CAMS][RINGBUF_SIZE];
static int sizes[MAX_CAMS][RINGBUF_SIZE];
static int ringbuf_most_recent_index[MAX_CAMS];
static int ringbuf_total_entries[MAX_CAMS];
static int ringbuf_has_discarded_first[MAX_CAMS]; // set to 1 after discarding the first frame


static void _print_usage(void) {
    printf("\n\
    \n\
    Print out camera data from Modal Pipe Architecture.\n\
    \n\
    Options are:\n\
    -a, --all        show all available cameras (not usable with test mode)\n\
    -h, --help       print this help message\n\
    -n, --newline    print each sample on a new line(not usable with --all)\n\
    -t, --test       test camera feedback (one at a time)\n\
    \n\
    typical usage:\n\
    /# voxl-inspect-cam tracking\n\
    /# voxl-inspect-cam tracking hires\n\
    \n");
    return;
}

static bool already_used(char *name){
    for(int i = 0; i < num_pipes; i++){
         if(!strcmp(name, pipe_path[i])){
            //printf("Already in use: %s\n", name);
            return true;
        }
    }
    //printf("Not in use: %s\n", name);
    return false;
}

static int _parse_opts(int argc, char* argv[])
{
    static struct option long_options[] =
    {
        {"all",                no_argument,        0, 'a'},
        {"help",               no_argument,        0, 'h'},
        {"newline",            no_argument,        0, 'n'},
        {"test",               no_argument,        0, 't'},
        {0, 0, 0, 0}
    };


    while(1){
        int option_index = 0;
        int c = getopt_long(argc, argv, "ahnt", long_options, &option_index);

        if(c == -1) break; // Detect the end of the options.

        switch(c){
        case 0:
            // for long args without short equivalent that just set a flag
            // nothing left to do so just break.
            if (long_options[option_index].flag != 0) break;
            break;
        case 'a':
            use_all = true;
            if(en_newline){
                fprintf(stderr, "ERROR, can't use --newline with --all\n");
                return -1;
            }
            if(test_mode){
                fprintf(stderr, "ERROR, can't use test_mode with --all\n");
                return -1;
            }
            break;
        case 'h':
            _print_usage();
            exit(0);
        case 'n':
            en_newline = 1;
            if(use_all){
                fprintf(stderr, "ERROR, can't use --newline with --all\n");
                return -1;
            }
            break;
        case 't':
            test_mode = true;
            if(use_all){
                fprintf(stderr, "ERROR, can't use test_mode with --all\n");
                return -1;
            }
            break;
        default:
            _print_usage();
            exit(0);
        }
    }

    if(!use_all){
        // scan through the non-flagged arguments for the desired pipe
        for(int i=optind; i<argc; i++){

            if(already_used(argv[i])) continue;

            if(pipe_expand_location_string(argv[i], pipe_path[num_pipes])<0){
                fprintf(stderr, "ERROR: Invalid pipe name: %s\n", argv[i]);
                exit(-1);
            }

            strcpy(pipe_path[num_pipes], argv[i]);
            max_name_length = max(max_name_length, (int)strlen(argv[i]));

            num_pipes++;
        }

        // make sure a pipe was given
        if(pipe_path[0][0] == 0){
            fprintf(stderr, "ERROR: There must be at least one pipe\n");
            _print_usage();
            exit(-1);
        }
    }

    return 0;
}


// Calculate the average framerate from the last RINGBUF_SIZE frames
static double _calc_avg_fr_hz(int ch)
{
    // don't bother calculating until we have some data to show
    if(ringbuf_total_entries[ch]<RINGBUF_WAKEUP) return -1.0;

    // find time from the oldest to the newest
    int idx_oldest = ringbuf_most_recent_index[ch]-ringbuf_total_entries[ch]+1;
    if(idx_oldest<0) idx_oldest += RINGBUF_SIZE;
    int64_t current_t = times[ch][ringbuf_most_recent_index[ch]];
    int64_t old_t = times[ch][idx_oldest];
    double time_s = ((double)(current_t-old_t))/1000000000.0;

    return (ringbuf_total_entries[ch]-1) / time_s;
}


static double _calc_avg_latency_ms(int ch)
{
    // don't bother calculating until we have some data to show
    if(ringbuf_total_entries[ch]<RINGBUF_WAKEUP) return -1.0;


    // sum all the bytes we have in the ringbuffer not including the oldest
    uint64_t total = 0;
    for(int i=0; i<ringbuf_total_entries[ch]; i++){
        int this_idx = (ringbuf_most_recent_index[ch]-i);
        if(this_idx<0) this_idx += RINGBUF_SIZE;
        total += latencies[ch][this_idx];
    }
    total = total/ringbuf_total_entries[ch];

    return (double)total/1000000.0;
}


static double _calc_bitrate_mbps(int ch)
{
    // don't bother calculating until we have some data to show
    if(ringbuf_total_entries[ch]<RINGBUF_WAKEUP) return -1.0;

    // sum all the bytes we have in the ringbuffer not including the oldest
    uint64_t total_bytes = 0;
    for(int i=0; i<(ringbuf_total_entries[ch]-1); i++){
        int this_idx = (ringbuf_most_recent_index[ch]-i);
        if(this_idx<0) this_idx += RINGBUF_SIZE;
        total_bytes += sizes[ch][this_idx];
    }

    // find time from the oldest to the newest
    int idx_oldest = (ringbuf_most_recent_index[ch]-ringbuf_total_entries[ch])+1;
    if(idx_oldest<0) idx_oldest += RINGBUF_SIZE;
    int64_t current_t = times[ch][ringbuf_most_recent_index[ch]];
    int64_t old_t = times[ch][idx_oldest];
    double time_s = ((double)(current_t-old_t))/1000000000.0;

    // finally divide to find mbps
    double mbps = 8.0 * ((double)(total_bytes)/1000000.0) / time_s;
    return mbps;
}


// called whenever we disconnect from the server
static void _disconnect_cb(int ch, __attribute__((unused)) void* context)
{
    gotoxy(0, ch + 1);
    printf(CLEAR_LINE"| %*s | "FONT_BLINK "Server Disconnected" RESET_FONT, max_name_length, pipe_path[ch]);
    fflush(stdout);
    return;
}

static void _print_format(int format, char* data)
{
    switch(format){
    case IMAGE_FORMAT_H264 :
        switch (data[4]) {
            case 0x67 : //Header
                printf("| H264 (head)");
                return;
            case 0x65 : // I Frame
                printf("| H264 (I)   ");
                return;
            case 0x41 : // P Frame
                printf("| H264 (P)   ");
                return;
            default   : // TODO test to see what a b frame is
                printf("| H264       ");
                return;
        }
        break;
    case IMAGE_FORMAT_H265 :
        switch (data[4]) {
            case 0x40 : //Header
                printf("| H265 (head)");
                return;
            case 0x26 : // I Frame
                printf("| H265 (I)   ");
                return;
            case 0x02 : // P Frame
                printf("| H265 (P)   ");
                return;
            default   : // TODO test to see what a b frame is
                printf("| H265       ");
                return;
        }
        break;
    default :
        printf("| %s", pipe_image_format_to_string(format));
    }

    return;
}

// camera helper callback whenever a frame arrives
static void _helper_cb(int ch, camera_image_metadata_t meta, __attribute__((unused))char* frame, __attribute__((unused)) void* context)
{
    // skip the first frame as it might be an h264 header, not real data
    // also the first frame may have unusual latency
    // still print to the screen, but don't record in the ringbuffer
    if(ringbuf_has_discarded_first[ch]){
        // enter data into the ringbuffers
        ringbuf_total_entries[ch]++;
        if(ringbuf_total_entries[ch]>RINGBUF_SIZE){
            ringbuf_total_entries[ch] = RINGBUF_SIZE;
        }
        if(ringbuf_total_entries[ch]==1){
            ringbuf_most_recent_index[ch]=-1;
        }
        ringbuf_most_recent_index[ch]++;
        if(ringbuf_most_recent_index[ch] >= RINGBUF_SIZE){
            ringbuf_most_recent_index[ch] = 0;
        }

        uint64_t t_now = _time_monotonic_ns();
        sizes[ch][ringbuf_most_recent_index[ch]] = meta.size_bytes;
        latencies[ch][ringbuf_most_recent_index[ch]] = t_now-(meta.timestamp_ns+meta.exposure_ns);

        // if timestamp is wrong, just use current time
        uint64_t t_for_buf = meta.timestamp_ns;

        if(t_for_buf>t_now || (t_now-t_for_buf)>1000000000){
            //printf("BAD TIMESTAMP");
            t_for_buf = t_now;
        }
        times[ch][ringbuf_most_recent_index[ch]] = t_for_buf;
    }
    ringbuf_has_discarded_first[ch] = 1;


    // make sure we are only printing one line at a time to the screen
    pthread_mutex_lock(&mutex);

    if(en_newline) printf("\n| %*s ", max_name_length, pipe_path[ch]);
    else gotoxy(max_name_length + 3, ch + 1);

    printf("|%8d |%5d |%5d |%6.2f |%5d |%9d ",
        meta.size_bytes,
        meta.width,
        meta.height,
        meta.exposure_ns/1000000.0,
        meta.gain,
        meta.frame_id);

    double latency = _calc_avg_latency_ms(ch);
    if(latency>0) printf("|%9.1f  ", latency);
    else printf("|           ");

    double fps = _calc_avg_fr_hz(ch);
    if(fps>0) printf("|%5.1f ", fps);
    else printf("|      ");

    double mbps = _calc_bitrate_mbps(ch);
    if(mbps>0) printf("|%7.1f ", mbps);
    else printf("|        ");

    _print_format(meta.format, frame);

    _calc_bitrate_mbps(ch);

    if(use_all) gotoxy(0, num_pipes + 1);

    pthread_mutex_unlock(&mutex);
    fflush(stdout);

        // if in test mode, verify bytes, exposure, etc are not zero
    if(test_mode && meta.size_bytes > 0){
        main_running = 0;
        pipe_client_set_camera_helper_cb(ch, NULL, NULL);
        test_passed = 1;
        return;
    }

    return;
}


static void _print_header(int max_name_length)
{
    printf(CLEAR_TERMINAL FONT_BOLD DISABLE_WRAP);
    printf("| %*s |  bytes  | wide |  hgt |exp(ms)| gain | frame id |latency(ms)|  fps |  mbps  | format\n", max_name_length, "Pipe Name");
    printf(RESET_FONT);
    return;
}


static void* list_checker_thread(__attribute__((unused))void *data){

    FILE *fp;
    char buffer[128];

    while (main_running) {

        /* Open the command for reading. */
        fp = popen("voxl-list-pipes -t camera_image_metadata_t", "r");
        if (fp == NULL) {
            printf("Failed to list cameras\n" );
            exit(-1);
        }

        // Read the output a line at a time
        while (fgets(buffer, sizeof(buffer), fp) != NULL) {
            buffer[strlen(buffer) - 1] = 0;
            //printf("%s\n", buffer);
            if(!already_used(buffer)){
                strcpy(pipe_path[num_pipes], buffer);

                pthread_mutex_lock(&mutex);
                //Have to update all the formatting if we get a longer name :(
                if(((int)strlen(pipe_path[num_pipes])) > max_name_length){

                    max_name_length = (int)strlen(pipe_path[num_pipes]);

                    _print_header(max_name_length);

                    for(int i = 0; i <= num_pipes; i++){
                        printf("| %*s |\n", max_name_length, pipe_path[i]);
                    }
                } else {
                    gotoxy(0, num_pipes + 1);
                    printf("| %*s |\n", max_name_length, pipe_path[num_pipes]);
                }
                pthread_mutex_unlock(&mutex);

                pipe_client_set_camera_helper_cb(num_pipes, _helper_cb, NULL);
                pipe_client_set_disconnect_cb(num_pipes, _disconnect_cb, NULL);
                pipe_client_open(num_pipes, pipe_path[num_pipes], CLIENT_NAME, EN_PIPE_CLIENT_CAMERA_HELPER, 0);

                num_pipes++;
            }
        }

        /* close */
        pclose(fp);

        sleep(1);
    }

    return NULL;

}

int main(int argc, char* argv[])
{
    // check for options
    if(_parse_opts(argc, argv)) return -1;

    // set some basic signal handling for safe shutdown.
    // quitting without cleanup up the pipe can result in the pipe staying
    // open and overflowing, so always cleanup properly!!!
    enable_signal_handler();
    main_running = 1;

    // set up all our MPA callbacks
    //pipe_client_set_camera_helper_cb(0, _helper_cb, NULL);
    //pipe_client_set_disconnect_cb(0, _disconnect_cb, NULL);

    _print_header(max_name_length);

    pthread_mutex_init(&mutex, NULL);

    if(use_all){
        pthread_create(&parser_thread, NULL, list_checker_thread, NULL);
    } else {

        for(int i = 0; i < num_pipes; i++){

            pthread_mutex_lock(&mutex);
            gotoxy(0, i + 1);
            printf("| %*s | Waiting for Server" RESET_FONT, max_name_length, pipe_path[i]);
            pthread_mutex_unlock(&mutex);

            pipe_client_set_camera_helper_cb(i, _helper_cb, NULL);
            pipe_client_set_disconnect_cb(i, _disconnect_cb, NULL);
            pipe_client_open(i, pipe_path[i], CLIENT_NAME, EN_PIPE_CLIENT_CAMERA_HELPER, 0);

        }

    }

    // request a new pipe from the server
    //printf(CLEAR_TERMINAL "waiting for server at %s\n", pipe_path);
    //int ret = pipe_client_open(0, pipe_path, CLIENT_NAME, EN_PIPE_CLIENT_CAMERA_HELPER, 0);

    // keep going until the  signal handler sets the running flag to 0

    // if no frame is recieved within 10 seconds, exit with error 1
    int i = 0;
    while(main_running)
    {
        // test failed, quit
        if((test_mode) && (i++ > 20)){
            main_running = 0;
            test_passed = 0;
        }
        usleep(500000);
    }

    // all done, signal pipe read threads to stop
    pipe_client_close_all();

    if(test_mode){
        if(test_passed){
            printf("\n\nTEST PASSED\n");
            return 0;
        }
        else{
            printf("\n\nTEST FAILED\n");
            return -1;
        }
    }

    gotoxy(0, num_pipes + 3);

    printf("\nclosing and exiting\n" RESET_FONT ENABLE_WRAP);
    return 0;
}
