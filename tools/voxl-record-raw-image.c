/*******************************************************************************
 * Copyright 2023 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <pthread.h>
#include <stdio.h>
#include <signal.h>
#include <getopt.h>
#include <time.h>
#include <unistd.h>	// for usleep()
#include <string.h>
#include <stdlib.h> // for atoi()
#include <math.h>
#include <sys/stat.h>   // for mkdir
#include <sys/types.h>  // for mode_t in mkdir
#include <errno.h>

#include <modal_start_stop.h>
#include <modal_pipe_client.h>

#include "common.h"

// TODO make a png version where possible


#define CLIENT_NAME		"voxl-record-raw-image"
#define DEFAULT_DIR		"/data/raw-images/"

static char pipe_path[MODAL_PIPE_MAX_PATH_LEN];
static char base_dir[MODAL_PIPE_MAX_PATH_LEN];
static char short_pipe_name[MODAL_PIPE_MAX_PATH_LEN];

static int n = 1;
static int current_n = 1;

static int is_multiple_frames = 0;
static int is_custom_dir = 0;


static int _mkdir_recursive(const char* dir)
{
	char tmp[MODAL_PIPE_MAX_PATH_LEN];
	char* p = NULL;

	snprintf(tmp, sizeof(tmp),"%s",dir);
	for(p = tmp + 1; *p!=0; p++){
		if(*p == '/'){
			*p = 0;
			if(mkdir(tmp, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) && errno!=EEXIST){
				perror("ERROR calling mkdir");
				printf("tried to make %s\n", tmp);
				return -1;
			}
			*p = '/';
		}
	}
	return 0;
}


static void _print_usage(void) {
printf("\n\
\n\
Record a raw image from an MPA pipe to disk\n\
This is typically used for inspecting raw or YUV\n\
image data formatting or for loading into something\n\
like MATLAB or OpenCV for post-processing.\n\
\n\
Optional arguments are:\n\
-d, --dir {dir}       directory to save output files in (default: /data/raw-images/)\n\
-h, --help            print this help message\n\
-n, --num-images {n}  number of images to save from the pipe (default 1)\n\
\n\
typical usage:\n\
/# voxl-record-video tracking\n\
/# voxl-record-video tracking -d /data/tracking_test_1/ -n 10\n\
\n");
	return;
}



static int _parse_opts(int argc, char* argv[])
{
	static struct option long_options[] =
	{
		{"dir",                required_argument,        0, 'd'},
		{"help",               no_argument,              0, 'h'},
		{"num-images",         required_argument,        0, 'n'},
		{0, 0, 0, 0}
	};


	while(1){
		int option_index = 0;
		int c = getopt_long(argc, argv, "d:hn:", long_options, &option_index);

		if(c == -1) break; // Detect the end of the options.

		switch(c){
		case 0:
			// for long args without short equivalent that just set a flag
			// nothing left to do so just break.
			if (long_options[option_index].flag != 0) break;
			break;
		case 'd':
			// TODO path validation
			strcpy(base_dir, optarg);
			is_custom_dir = 1;
			break;
		case 'h':
			_print_usage();
			exit(0);
			break;
		case 'n':
			n = atoi(optarg);
			if(n<1){
				fprintf(stderr, "n must be >= 1\n");
				exit(1);
			}
			if(n>1) is_multiple_frames = 1;
			current_n = n;
			break;
		default:
			_print_usage();
			exit(0);
		}
	}

	// scan through the non-flagged arguments for the desired pipe
	for(int i=optind; i<argc; i++){
		if(pipe_path[0]!=0){
			fprintf(stderr, "ERROR: Please specify only one pipe\n");
			_print_usage();
			exit(-1);
		}
		if(pipe_expand_location_string(argv[i], pipe_path)<0){
			fprintf(stderr, "ERROR: Invalid pipe name: %s\n", argv[i]);
			exit(-1);
		}
		strcpy(short_pipe_name, argv[i]);
	}

	// make sure a pipe was given
	if(pipe_path[0] == 0){
		fprintf(stderr, "ERROR: There must be one input pipe argument given\n");
		_print_usage();
		exit(-1);
	}

	return 0;
}



// called whenever we disconnect from the server
static void _disconnect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
	printf("Server Disconnected\n");
	return;
}


// These are extensions chosen to work with imagemagick
// raw images don't have consistent file types like png or tiff
// TODO see how imagemagick deals with NV12 and YUV
static const char* _format_to_ext(int fmt)
{
	switch(fmt){
		case IMAGE_FORMAT_RAW8:
			return "gray";
		case IMAGE_FORMAT_STEREO_RAW8:
			return "gray";
		case IMAGE_FORMAT_H264:
			return "h264";
		case IMAGE_FORMAT_H265:
			return "h265";
		case IMAGE_FORMAT_RAW16:
			return "gray";
		case IMAGE_FORMAT_NV21:
			return "nv21";
		case IMAGE_FORMAT_JPG:
			return "jpg";
		case IMAGE_FORMAT_YUV422:
			return "yuv422";
		case IMAGE_FORMAT_YUV420:
			return "yuv420";
		case IMAGE_FORMAT_RGB:
			return "rgb";
		case IMAGE_FORMAT_FLOAT32:
			return "f32";
		case IMAGE_FORMAT_STEREO_NV21:
			return "nv21";
		case IMAGE_FORMAT_STEREO_RGB :
			return "rgb";
		case IMAGE_FORMAT_YUV422_UYVY:
			return "yuv422";
		case IMAGE_FORMAT_STEREO_NV12:
			return "nv12";
		default:
			return "bin";
	}
}

static void _print_instructions(int fmt, const char* filename, const char* dir, int w, int h)
{
	switch(fmt){
		case IMAGE_FORMAT_RAW8:
		case IMAGE_FORMAT_RGB:
		case IMAGE_FORMAT_STEREO_RAW8:
		case IMAGE_FORMAT_STEREO_RGB:
			printf("\nTo view this image, use adb pull to pull it to your desktop\n");
			printf("adb pull %s%s\n", dir, filename);
			printf("\nThen use the free imagemagick tool to view it\n");
			printf("display -size %dx%d -depth 8 %s\n", w, h, filename);
			return;

		case IMAGE_FORMAT_RAW16:
			printf("\nTo view this image, use adb pull to pull it to your desktop\n");
			printf("adb pull %s%s\n", dir, filename);
			printf("\nThen use the free imagemagick tool to view it\n");
			printf("display -size %dx%d -depth 16 %s\n", w, h, filename);
			return;

		case IMAGE_FORMAT_H264:
		case IMAGE_FORMAT_H265:
			printf("\nSuccessfully saved one or more frame of a video stream\n");
			printf("You won't be able to view individual frames, but the first\n");
			printf("file should be a header frame which can be decoded with the\n");
			printf("free tool called mediainfo-gui\n");
			return;

		default:
			return;
	}
}

// camera helper callback whenever a frame arrives
static void _helper_cb(__attribute__((unused)) int ch, camera_image_metadata_t meta, __attribute__((unused))char* frame, __attribute__((unused)) void* context)
{

	char name[MODAL_PIPE_MAX_PATH_LEN] = "";
	char full_path[MODAL_PIPE_MAX_PATH_LEN] = "";

	int f = meta.format;
	int w = meta.width;
	int h = meta.height;

	// double the height for stereo pairs
	if(	f == IMAGE_FORMAT_STEREO_RAW8 || \
		f == IMAGE_FORMAT_STEREO_NV21 || \
		f == IMAGE_FORMAT_STEREO_RGB  || \
		f == IMAGE_FORMAT_STEREO_NV12)
	{
		h = h*2;
	}

	if(is_multiple_frames){
		snprintf(name, sizeof(name)-1, "%s_%dx%d_#%04d.%s", \
			short_pipe_name, w, h,\
			(n - current_n),\
			_format_to_ext(f));
	}
	else{
		snprintf(name, sizeof(name)-1, "%s_%dx%d.%s", \
			short_pipe_name, w, h,\
			_format_to_ext(f));
	}

	snprintf(full_path, sizeof(full_path)-1, "%s%s", base_dir, name);


	printf("writing to: %s\n", full_path);


	FILE* file = fopen(full_path, "wb");
	if(! file){
		fprintf(stderr, "ERROR: Failed to make file: \n");
		pipe_client_close_all();
		exit(-1);
	}

	int ret = fwrite(frame, meta.size_bytes, 1, file);
	fclose(file);

	if(ret!=1){
		perror("failed to write to disk");
		pipe_client_close_all();
		exit(-1);
	}

	// decrement counter and see if it's time to quit
	current_n--;
	if(current_n<1){
		pipe_client_close_all();
		_print_instructions(f, name, base_dir, w, h);
		exit(0);
	}

	return;
}




int main(int argc, char* argv[])
{
	// check for options
	if(_parse_opts(argc, argv)) return -1;

	// set some basic signal handling for safe shutdown.
	// quitting without cleanup up the pipe can result in the pipe staying
	// open and overflowing, so always cleanup properly!!!
	enable_signal_handler();
	main_running = 1;


	// search for existing log folders to determine the next number in the series
	if(!is_custom_dir){
		_mkdir_recursive(DEFAULT_DIR);
		for(int i=0; i<=10000; i++){

			// make the next log directory in the sequence
			sprintf(base_dir, DEFAULT_DIR "%04d/", i);

			// use basic mkdir to try making this directory
			int ret = mkdir(base_dir, S_IRWXU);
			if(ret==0){
				printf("successfully made new dir: %s\n", base_dir);
				break;
			}
			if(errno!=EEXIST){
				perror("error searching for next directory in sequence");
				return -1;
			}
			// dir exists, move onto the next one
		}
	}
	else{
		// user gave a custom dir
		if(_mkdir_recursive(base_dir)) exit(-1);
	}

	// set up all our MPA callbacks
	pipe_client_set_camera_helper_cb(0, _helper_cb, NULL);
	pipe_client_set_disconnect_cb(0, _disconnect_cb, NULL);
	printf(CLEAR_TERMINAL "waiting for server at %s\n", pipe_path);
	pipe_client_open(0, pipe_path, CLIENT_NAME, EN_PIPE_CLIENT_CAMERA_HELPER, 0);

	// keep going until the  signal handler sets the running flag to 0
	while(main_running) usleep(500000);

	// all done, signal pipe read threads to stop
	pipe_client_close_all();
	printf("\nclosing and exiting\n" RESET_FONT ENABLE_WRAP);

	return 0;
}
