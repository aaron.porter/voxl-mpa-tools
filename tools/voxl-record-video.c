/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <pthread.h>
#include <stdio.h>
#include <signal.h>
#include <getopt.h>
#include <time.h>
#include <unistd.h>	// for usleep()
#include <string.h>
#include <stdlib.h> // for atoi()
#include <math.h>
#include <sys/stat.h>   // for mkdir
#include <sys/types.h>  // for mode_t in mkdir
#include <errno.h>

#include <modal_start_stop.h>
#include <modal_pipe_client.h>

#include "common.h"



#define CLIENT_NAME		"voxl-record-video"

static char pipe_path[MODAL_PIPE_MAX_PATH_LEN];
static char out_path[MODAL_PIPE_MAX_PATH_LEN] = "/data/video/voxl-record-video.h264";
FILE * encoded_out;



static int _mkdir_recursive(const char* dir)
{
	char tmp[MODAL_PIPE_MAX_PATH_LEN];
	char* p = NULL;

	snprintf(tmp, sizeof(tmp),"%s",dir);
	for(p = tmp + 1; *p!=0; p++){
		if(*p == '/'){
			*p = 0;
			if(mkdir(tmp, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) && errno!=EEXIST){
				perror("ERROR calling mkdir");
				printf("tried to make %s\n", tmp);
				return -1;
			}
			*p = '/';
		}
	}
	return 0;
}


static void _print_usage(void) {
printf("\n\
\n\
Record H264/H265 stream from pipe to disk\n\
\n\
Options are:\n\
-o, --out        path for output file\n\
-h, --help       print this help message\n\
\n\
typical usage:\n\
/# voxl-record-video hires\n\
/# voxl-record-video hires /data/vid1.h264\n\
\n");
	return;
}



static int _parse_opts(int argc, char* argv[])
{
	static struct option long_options[] =
	{
		{"out",                required_argument,        0, 'o'},
		{"help",               no_argument,        0, 'h'},
		{0, 0, 0, 0}
	};


	while(1){
		int option_index = 0;
		int c = getopt_long(argc, argv, "o:h", long_options, &option_index);

		if(c == -1) break; // Detect the end of the options.

		switch(c){
		case 0:
			// for long args without short equivalent that just set a flag
			// nothing left to do so just break.
			if (long_options[option_index].flag != 0) break;
			break;
		case 'o':
			// TODO path validation
			strcpy(out_path, optarg);
			break;
		case 'h':
			_print_usage();
			exit(0);
		default:
			_print_usage();
			exit(0);
		}
	}

	// scan through the non-flagged arguments for the desired pipe
	for(int i=optind; i<argc; i++){
		if(pipe_path[0]!=0){
			fprintf(stderr, "ERROR: Please specify only one pipe\n");
			_print_usage();
			exit(-1);
		}
		if(pipe_expand_location_string(argv[i], pipe_path)<0){
			fprintf(stderr, "ERROR: Invalid pipe name: %s\n", argv[i]);
			exit(-1);
		}
	}

	// make sure a pipe was given
	if(pipe_path[0] == 0){
		fprintf(stderr, "ERROR: There must be one input pipe argument given\n");
		_print_usage();
		exit(-1);
	}

	return 0;
}



// called whenever we disconnect from the server
static void _disconnect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
	printf("Server Disconnected\n");
	return;
}


// camera helper callback whenever a frame arrives
static void _helper_cb(__attribute__((unused)) int ch, camera_image_metadata_t meta, __attribute__((unused))char* frame, __attribute__((unused)) void* context)
{

	if(meta.format != IMAGE_FORMAT_H264 && meta.format != IMAGE_FORMAT_H265){
		fprintf(stderr, "error, can only record pipes with type h264 or h265\n");
		pipe_client_close_all();
		exit(-1);
	}

	if(! encoded_out){
		if(_mkdir_recursive(out_path)){
			pipe_client_close_all();
			exit(-1);
		}

		// wb: If the file exists, its contents are cleared
		encoded_out = fopen(out_path, "wb");
		if(! encoded_out){
			printf("Failed to open test output file: %s\n", out_path);
			pipe_client_close_all();
			exit(-1);
		} else {
			printf("Opened output file: %s\n", out_path);
		}
	}

	int ret = fwrite(frame, meta.size_bytes, 1, encoded_out);

	if(ret!=1){
		perror("failed to write to disk");
		pipe_client_close_all();
		exit(-1);
	}


	return;
}

static void __attribute__((destructor)) destroy(){
	if(encoded_out)
		fclose(encoded_out);
}


int main(int argc, char* argv[])
{
	// check for options
	if(_parse_opts(argc, argv)) return -1;

	// set some basic signal handling for safe shutdown.
	// quitting without cleanup up the pipe can result in the pipe staying
	// open and overflowing, so always cleanup properly!!!
	enable_signal_handler();
	main_running = 1;

	// set up all our MPA callbacks
	pipe_client_set_camera_helper_cb(0, _helper_cb, NULL);
	pipe_client_set_disconnect_cb(0, _disconnect_cb, NULL);
	printf(CLEAR_TERMINAL "waiting for server at %s\n", pipe_path);
	pipe_client_open(0, pipe_path, CLIENT_NAME, EN_PIPE_CLIENT_CAMERA_HELPER, 0);

	// keep going until the  signal handler sets the running flag to 0
	while(main_running) usleep(500000);

	// all done, signal pipe read threads to stop
	pipe_client_close_all();
	printf("\nclosing and exiting\n" RESET_FONT ENABLE_WRAP);

	// destructor will close file now
	return 0;
}
