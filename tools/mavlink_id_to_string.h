/*******************************************************************************
 * Copyright 2023 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#ifndef MAVLINK_ID_TO_STRING
#define MAVLINK_ID_TO_STRING


#include <string.h>
#include <c_library_v2/common/mavlink.h>

// TODO replace strcpy with strncpy for safety
#define MAX_MAVLINK_NAME_LEN 64

void mavlink_id_to_string(uint32_t id, char* mav_name);

void mavlink_id_to_string(uint32_t id, char* mav_name)
{
    if(mav_name==NULL) return;

    switch(id){
        case MAVLINK_MSG_ID_HEARTBEAT:
            strcpy(mav_name, "heartbeat");
            break;
        case MAVLINK_MSG_ID_SYS_STATUS:
            strcpy(mav_name, "sys_status");
            break;
        case MAVLINK_MSG_ID_SYSTEM_TIME:
            strcpy(mav_name, "system_time");
            break;
        case MAVLINK_MSG_ID_PING:
            strcpy(mav_name, "ping");
            break;
        case MAVLINK_MSG_ID_CHANGE_OPERATOR_CONTROL:
            strcpy(mav_name, "change_operator_control");
            break;
        case MAVLINK_MSG_ID_CHANGE_OPERATOR_CONTROL_ACK:
            strcpy(mav_name, "change_operator_control_ack");
            break;
        case MAVLINK_MSG_ID_AUTH_KEY:
            strcpy(mav_name, "auth_key");
            break;
        case MAVLINK_MSG_ID_LINK_NODE_STATUS:
            strcpy(mav_name, "link_node_status");
            break;
        case MAVLINK_MSG_ID_SET_MODE:
            strcpy(mav_name, "set_mode");
            break;
        case MAVLINK_MSG_ID_PARAM_REQUEST_READ:
            strcpy(mav_name, "param_request_read");
            break;
        case MAVLINK_MSG_ID_PARAM_REQUEST_LIST:
            strcpy(mav_name, "param_request_list");
            break;
        case MAVLINK_MSG_ID_PARAM_VALUE:
            strcpy(mav_name, "param_value");
            break;
        case MAVLINK_MSG_ID_PARAM_SET:
            strcpy(mav_name, "param_set");
            break;
        case MAVLINK_MSG_ID_GPS_RAW_INT:
            strcpy(mav_name, "gps_raw_int");
            break;
        case MAVLINK_MSG_ID_GPS_STATUS:
            strcpy(mav_name, "gps_status");
            break;
        case MAVLINK_MSG_ID_SCALED_IMU:
            strcpy(mav_name, "scaled_imu");
            break;
        case MAVLINK_MSG_ID_RAW_IMU:
            strcpy(mav_name, "raw_imu");
            break;
        case MAVLINK_MSG_ID_RAW_PRESSURE:
            strcpy(mav_name, "raw_pressure");
            break;
        case MAVLINK_MSG_ID_SCALED_PRESSURE:
            strcpy(mav_name, "scaled_pressure");
            break;
        case MAVLINK_MSG_ID_ATTITUDE:
            strcpy(mav_name, "attitude");
            break;
        case MAVLINK_MSG_ID_ATTITUDE_QUATERNION:
            strcpy(mav_name, "attitude_quaternion");
            break;
        case MAVLINK_MSG_ID_LOCAL_POSITION_NED:
            strcpy(mav_name, "local_position_ned");
            break;
        case MAVLINK_MSG_ID_GLOBAL_POSITION_INT:
            strcpy(mav_name, "global_position_int");
            break;
        case MAVLINK_MSG_ID_RC_CHANNELS_SCALED:
            strcpy(mav_name, "rc_channels_scaled");
            break;
        case MAVLINK_MSG_ID_RC_CHANNELS_RAW:
            strcpy(mav_name, "rc_channels_raw");
            break;
        case MAVLINK_MSG_ID_SERVO_OUTPUT_RAW:
            strcpy(mav_name, "servo_output_raw");
            break;
        case MAVLINK_MSG_ID_MISSION_REQUEST_PARTIAL_LIST:
            strcpy(mav_name, "mission_request_partial_list");
            break;
        case MAVLINK_MSG_ID_MISSION_WRITE_PARTIAL_LIST:
            strcpy(mav_name, "mission_write_partial_list");
            break;
        case MAVLINK_MSG_ID_MISSION_ITEM:
            strcpy(mav_name, "mission_item");
            break;
        case MAVLINK_MSG_ID_MISSION_REQUEST:
            strcpy(mav_name, "mission_request");
            break;
        case MAVLINK_MSG_ID_MISSION_SET_CURRENT:
            strcpy(mav_name, "mission_set_current");
            break;
        case MAVLINK_MSG_ID_MISSION_CURRENT:
            strcpy(mav_name, "mission_current");
            break;
        case MAVLINK_MSG_ID_MISSION_REQUEST_LIST:
            strcpy(mav_name, "mission_request_list");
            break;
        case MAVLINK_MSG_ID_MISSION_COUNT:
            strcpy(mav_name, "mission_count");
            break;
        case MAVLINK_MSG_ID_MISSION_CLEAR_ALL:
            strcpy(mav_name, "mission_clear_all");
            break;
        case MAVLINK_MSG_ID_MISSION_ITEM_REACHED:
            strcpy(mav_name, "mission_item_reached");
            break;
        case MAVLINK_MSG_ID_MISSION_ACK:
            strcpy(mav_name, "mission_ack");
            break;
        case MAVLINK_MSG_ID_SET_GPS_GLOBAL_ORIGIN:
            strcpy(mav_name, "set_gps_global_origin");
            break;
        case MAVLINK_MSG_ID_GPS_GLOBAL_ORIGIN:
            strcpy(mav_name, "gps_global_origin");
            break;
        case MAVLINK_MSG_ID_PARAM_MAP_RC:
            strcpy(mav_name, "param_map_rc");
            break;
        case MAVLINK_MSG_ID_MISSION_REQUEST_INT:
            strcpy(mav_name, "mission_request_int");
            break;
        case MAVLINK_MSG_ID_SAFETY_SET_ALLOWED_AREA:
            strcpy(mav_name, "safety_set_allowed_area");
            break;
        case MAVLINK_MSG_ID_SAFETY_ALLOWED_AREA:
            strcpy(mav_name, "safety_allowed_area");
            break;
        case MAVLINK_MSG_ID_ATTITUDE_QUATERNION_COV:
            strcpy(mav_name, "attitude_quaternion_cov");
            break;
        case MAVLINK_MSG_ID_NAV_CONTROLLER_OUTPUT:
            strcpy(mav_name, "nav_controller_output");
            break;
        case MAVLINK_MSG_ID_GLOBAL_POSITION_INT_COV:
            strcpy(mav_name, "global_position_int_cov");
            break;
        case MAVLINK_MSG_ID_LOCAL_POSITION_NED_COV:
            strcpy(mav_name, "local_position_ned_cov");
            break;
        case MAVLINK_MSG_ID_RC_CHANNELS:
            strcpy(mav_name, "rc_channels");
            break;
        case MAVLINK_MSG_ID_REQUEST_DATA_STREAM:
            strcpy(mav_name, "request_data_stream");
            break;
        case MAVLINK_MSG_ID_DATA_STREAM:
            strcpy(mav_name, "data_stream");
            break;
        case MAVLINK_MSG_ID_MANUAL_CONTROL:
            strcpy(mav_name, "manual_control");
            break;
        case MAVLINK_MSG_ID_RC_CHANNELS_OVERRIDE:
            strcpy(mav_name, "rc_channels_override");
            break;
        case MAVLINK_MSG_ID_MISSION_ITEM_INT:
            strcpy(mav_name, "mission_item_int");
            break;
        case MAVLINK_MSG_ID_VFR_HUD:
            strcpy(mav_name, "vfr_hud");
            break;
        case MAVLINK_MSG_ID_COMMAND_INT:
            strcpy(mav_name, "command_int");
            break;
        case MAVLINK_MSG_ID_COMMAND_LONG:
            strcpy(mav_name, "command_long");
            break;
        case MAVLINK_MSG_ID_COMMAND_ACK:
            strcpy(mav_name, "command_ack");
            break;
        case MAVLINK_MSG_ID_COMMAND_CANCEL:
            strcpy(mav_name, "command_cancel");
            break;
        case MAVLINK_MSG_ID_MANUAL_SETPOINT:
            strcpy(mav_name, "manual_setpoint");
            break;
        case MAVLINK_MSG_ID_SET_ATTITUDE_TARGET:
            strcpy(mav_name, "set_attitude_target");
            break;
        case MAVLINK_MSG_ID_ATTITUDE_TARGET:
            strcpy(mav_name, "attitude_target");
            break;
        case MAVLINK_MSG_ID_SET_POSITION_TARGET_LOCAL_NED:
            strcpy(mav_name, "set_position_target_local_ned");
            break;
        case MAVLINK_MSG_ID_POSITION_TARGET_LOCAL_NED:
            strcpy(mav_name, "position_target_local_ned");
            break;
        case MAVLINK_MSG_ID_SET_POSITION_TARGET_GLOBAL_INT:
            strcpy(mav_name, "set_position_target_global_int");
            break;
        case MAVLINK_MSG_ID_POSITION_TARGET_GLOBAL_INT:
            strcpy(mav_name, "position_target_global_int");
            break;
        case MAVLINK_MSG_ID_LOCAL_POSITION_NED_SYSTEM_GLOBAL_OFFSET:
            strcpy(mav_name, "local_position_ned_system_global_offset3");
            break;
        case MAVLINK_MSG_ID_HIL_STATE:
            strcpy(mav_name, "hil_state");
            break;
        case MAVLINK_MSG_ID_HIL_CONTROLS:
            strcpy(mav_name, "hil_controls");
            break;
        case MAVLINK_MSG_ID_HIL_RC_INPUTS_RAW:
            strcpy(mav_name, "hil_rc_inputs_raw");
            break;
        case MAVLINK_MSG_ID_HIL_ACTUATOR_CONTROLS:
            strcpy(mav_name, "hil_actuator_controls");
            break;
        case MAVLINK_MSG_ID_OPTICAL_FLOW:
            strcpy(mav_name, "optical_flow");
            break;
        case MAVLINK_MSG_ID_GLOBAL_VISION_POSITION_ESTIMATE:
            strcpy(mav_name, "global_vision_position_estimate");
            break;
        case MAVLINK_MSG_ID_VISION_POSITION_ESTIMATE:
            strcpy(mav_name, "vision_position_estimate");
            break;
        case MAVLINK_MSG_ID_VISION_SPEED_ESTIMATE:
            strcpy(mav_name, "vision_speed_estimate");
            break;
        case MAVLINK_MSG_ID_VICON_POSITION_ESTIMATE:
            strcpy(mav_name, "vicon_position_estimate");
            break;
        case MAVLINK_MSG_ID_HIGHRES_IMU:
            strcpy(mav_name, "highres_imu");
            break;
        case MAVLINK_MSG_ID_OPTICAL_FLOW_RAD:
            strcpy(mav_name, "optical_flow_rad");
            break;
        case MAVLINK_MSG_ID_HIL_SENSOR:
            strcpy(mav_name, "hil_sensor");
            break;
        case MAVLINK_MSG_ID_SIM_STATE:
            strcpy(mav_name, "sim_state");
            break;
        case MAVLINK_MSG_ID_RADIO_STATUS:
            strcpy(mav_name, "radio_status");
            break;
        case MAVLINK_MSG_ID_FILE_TRANSFER_PROTOCOL:
            strcpy(mav_name, "file_transfer_protocol");
            break;
        case MAVLINK_MSG_ID_TIMESYNC:
            strcpy(mav_name, "timesync");
            break;
        case MAVLINK_MSG_ID_CAMERA_TRIGGER:
            strcpy(mav_name, "camera_trigger");
            break;
        case MAVLINK_MSG_ID_HIL_GPS:
            strcpy(mav_name, "hil_gps");
            break;
        case MAVLINK_MSG_ID_HIL_OPTICAL_FLOW:
            strcpy(mav_name, "hil_optical_flow");
            break;
        case MAVLINK_MSG_ID_HIL_STATE_QUATERNION:
            strcpy(mav_name, "hil_state_quaternion");
            break;
        case MAVLINK_MSG_ID_SCALED_IMU2:
            strcpy(mav_name, "scaled_imu2");
            break;
        case MAVLINK_MSG_ID_LOG_REQUEST_LIST:
            strcpy(mav_name, "log_request_list");
            break;
        case MAVLINK_MSG_ID_LOG_ENTRY:
            strcpy(mav_name, "log_entry");
            break;
        case MAVLINK_MSG_ID_LOG_REQUEST_DATA:
            strcpy(mav_name, "log_request_data");
            break;
        case MAVLINK_MSG_ID_LOG_DATA:
            strcpy(mav_name, "log_data");
            break;
        case MAVLINK_MSG_ID_LOG_ERASE:
            strcpy(mav_name, "log_erase");
            break;
        case MAVLINK_MSG_ID_LOG_REQUEST_END:
            strcpy(mav_name, "log_request_end");
            break;
        case MAVLINK_MSG_ID_GPS_INJECT_DATA:
            strcpy(mav_name, "gps_inject_data");
            break;
        case MAVLINK_MSG_ID_GPS2_RAW:
            strcpy(mav_name, "gps2_raw");
            break;
        case MAVLINK_MSG_ID_POWER_STATUS:
            strcpy(mav_name, "power_status");
            break;
        case MAVLINK_MSG_ID_SERIAL_CONTROL:
            strcpy(mav_name, "serial_control");
            break;
        case MAVLINK_MSG_ID_GPS_RTK:
            strcpy(mav_name, "gps_rtk");
            break;
        case MAVLINK_MSG_ID_GPS2_RTK:
            strcpy(mav_name, "gps2_rtk");
            break;
        case MAVLINK_MSG_ID_SCALED_IMU3:
            strcpy(mav_name, "scaled_imu3");
            break;
        case MAVLINK_MSG_ID_DATA_TRANSMISSION_HANDSHAKE:
            strcpy(mav_name, "data_transmission_handshake");
            break;
        case MAVLINK_MSG_ID_ENCAPSULATED_DATA:
            strcpy(mav_name, "encapsulated_data");
            break;
        case MAVLINK_MSG_ID_DISTANCE_SENSOR:
            strcpy(mav_name, "distance_sensor");
            break;
        case MAVLINK_MSG_ID_TERRAIN_REQUEST:
            strcpy(mav_name, "terrain_request");
            break;
        case MAVLINK_MSG_ID_TERRAIN_DATA:
            strcpy(mav_name, "terrain_data");
            break;
        case MAVLINK_MSG_ID_TERRAIN_CHECK:
            strcpy(mav_name, "terrain_check");
            break;
        case MAVLINK_MSG_ID_TERRAIN_REPORT:
            strcpy(mav_name, "terrain_report");
            break;
        case MAVLINK_MSG_ID_SCALED_PRESSURE2:
            strcpy(mav_name, "scaled_pressure2");
            break;
        case MAVLINK_MSG_ID_ATT_POS_MOCAP:
            strcpy(mav_name, "att_pos_mocap");
            break;
        case MAVLINK_MSG_ID_SET_ACTUATOR_CONTROL_TARGET:
            strcpy(mav_name, "set_actuator_control_target");
            break;
        case MAVLINK_MSG_ID_ACTUATOR_CONTROL_TARGET:
            strcpy(mav_name, "actuator_control_target");
            break;
        case MAVLINK_MSG_ID_ALTITUDE:
            strcpy(mav_name, "altitude");
            break;
        case MAVLINK_MSG_ID_RESOURCE_REQUEST:
            strcpy(mav_name, "resource_request");
            break;
        case MAVLINK_MSG_ID_SCALED_PRESSURE3:
            strcpy(mav_name, "scaled_pressure3");
            break;
        case MAVLINK_MSG_ID_FOLLOW_TARGET:
            strcpy(mav_name, "follow_target");
            break;
        case MAVLINK_MSG_ID_CONTROL_SYSTEM_STATE:
            strcpy(mav_name, "control_system_state");
            break;
        case MAVLINK_MSG_ID_BATTERY_STATUS:
            strcpy(mav_name, "battery_status");
            break;
        case MAVLINK_MSG_ID_AUTOPILOT_VERSION:
            strcpy(mav_name, "autopilot_version");
            break;
        case MAVLINK_MSG_ID_LANDING_TARGET:
            strcpy(mav_name, "landing_target");
            break;
        case MAVLINK_MSG_ID_FENCE_STATUS:
            strcpy(mav_name, "fence_status");
            break;
        case MAVLINK_MSG_ID_MAG_CAL_REPORT:
            strcpy(mav_name, "mag_cal_report");
            break;
        case MAVLINK_MSG_ID_EFI_STATUS:
            strcpy(mav_name, "efi_status");
            break;
        case MAVLINK_MSG_ID_ESTIMATOR_STATUS:
            strcpy(mav_name, "estimator_status");
            break;
        case MAVLINK_MSG_ID_WIND_COV:
            strcpy(mav_name, "wind_cov");
            break;
        case MAVLINK_MSG_ID_GPS_INPUT:
            strcpy(mav_name, "gps_input");
            break;
        case MAVLINK_MSG_ID_GPS_RTCM_DATA:
            strcpy(mav_name, "gps_rtcm_data");
            break;
        case MAVLINK_MSG_ID_HIGH_LATENCY:
            strcpy(mav_name, "high_latency");
            break;
        case MAVLINK_MSG_ID_HIGH_LATENCY2:
            strcpy(mav_name, "high_latency2");
            break;
        case MAVLINK_MSG_ID_VIBRATION:
            strcpy(mav_name, "vibration");
            break;
        case MAVLINK_MSG_ID_HOME_POSITION:
            strcpy(mav_name, "home_position");
            break;
        case MAVLINK_MSG_ID_SET_HOME_POSITION:
            strcpy(mav_name, "set_home_position");
            break;
        case MAVLINK_MSG_ID_MESSAGE_INTERVAL:
            strcpy(mav_name, "message_interval");
            break;
        case MAVLINK_MSG_ID_EXTENDED_SYS_STATE:
            strcpy(mav_name, "extended_sys_state");
            break;
        case MAVLINK_MSG_ID_ADSB_VEHICLE:
            strcpy(mav_name, "adsb_vehicle");
            break;
        case MAVLINK_MSG_ID_COLLISION:
            strcpy(mav_name, "collision");
            break;
        case MAVLINK_MSG_ID_V2_EXTENSION:
            strcpy(mav_name, "v2_extension");
            break;
        case MAVLINK_MSG_ID_MEMORY_VECT:
            strcpy(mav_name, "memory_vect");
            break;
        case MAVLINK_MSG_ID_DEBUG_VECT:
            strcpy(mav_name, "debug_vect");
            break;
        case MAVLINK_MSG_ID_NAMED_VALUE_FLOAT:
            strcpy(mav_name, "named_value_float");
            break;
        case MAVLINK_MSG_ID_NAMED_VALUE_INT:
            strcpy(mav_name, "named_value_int");
            break;
        case MAVLINK_MSG_ID_STATUSTEXT:
            strcpy(mav_name, "statustext");
            break;
        case MAVLINK_MSG_ID_DEBUG:
            strcpy(mav_name, "debug");
            break;
        case MAVLINK_MSG_ID_SETUP_SIGNING:
            strcpy(mav_name, "setup_signing");
            break;
        case MAVLINK_MSG_ID_BUTTON_CHANGE:
            strcpy(mav_name, "button_change");
            break;
        case MAVLINK_MSG_ID_PLAY_TUNE:
            strcpy(mav_name, "play_tune");
            break;
        case MAVLINK_MSG_ID_CAMERA_INFORMATION:
            strcpy(mav_name, "camera_information");
            break;
        case MAVLINK_MSG_ID_CAMERA_SETTINGS:
            strcpy(mav_name, "camera_settings");
            break;
        case MAVLINK_MSG_ID_STORAGE_INFORMATION:
            strcpy(mav_name, "storage_information");
            break;
        case MAVLINK_MSG_ID_CAMERA_CAPTURE_STATUS:
            strcpy(mav_name, "camera_capture_status");
            break;
        case MAVLINK_MSG_ID_CAMERA_IMAGE_CAPTURED:
            strcpy(mav_name, "camera_image_captured");
            break;
        case MAVLINK_MSG_ID_FLIGHT_INFORMATION:
            strcpy(mav_name, "flight_information");
            break;
        case MAVLINK_MSG_ID_MOUNT_ORIENTATION:
            strcpy(mav_name, "mount_orientation");
            break;
        case MAVLINK_MSG_ID_LOGGING_DATA:
            strcpy(mav_name, "logging_data");
            break;
        case MAVLINK_MSG_ID_LOGGING_DATA_ACKED:
            strcpy(mav_name, "logging_data_acked");
            break;
        case MAVLINK_MSG_ID_LOGGING_ACK:
            strcpy(mav_name, "logging_ack");
            break;
        case MAVLINK_MSG_ID_VIDEO_STREAM_INFORMATION:
            strcpy(mav_name, "video_stream_information");
            break;
        case MAVLINK_MSG_ID_VIDEO_STREAM_STATUS:
            strcpy(mav_name, "video_stream_status");
            break;
        case MAVLINK_MSG_ID_CAMERA_FOV_STATUS:
            strcpy(mav_name, "camera_fov_status");
            break;
        case MAVLINK_MSG_ID_CAMERA_TRACKING_IMAGE_STATUS:
            strcpy(mav_name, "camera_tracking_image_status");
            break;
        case MAVLINK_MSG_ID_CAMERA_TRACKING_GEO_STATUS:
            strcpy(mav_name, "camera_tracking_geo_status");
            break;
        case MAVLINK_MSG_ID_GIMBAL_MANAGER_INFORMATION:
            strcpy(mav_name, "gimbal_manager_information");
            break;
        case MAVLINK_MSG_ID_GIMBAL_MANAGER_STATUS:
            strcpy(mav_name, "gimbal_manager_status");
            break;
        case MAVLINK_MSG_ID_GIMBAL_MANAGER_SET_ATTITUDE:
            strcpy(mav_name, "gimbal_manager_set_attitude");
            break;
        case MAVLINK_MSG_ID_GIMBAL_DEVICE_INFORMATION:
            strcpy(mav_name, "gimbal_device_information");
            break;
        case MAVLINK_MSG_ID_GIMBAL_DEVICE_SET_ATTITUDE:
            strcpy(mav_name, "gimbal_device_set_attitude");
            break;
        case MAVLINK_MSG_ID_GIMBAL_DEVICE_ATTITUDE_STATUS:
            strcpy(mav_name, "gimbal_device_attitude_status");
            break;
        case MAVLINK_MSG_ID_AUTOPILOT_STATE_FOR_GIMBAL_DEVICE:
            strcpy(mav_name, "autopilot_state_for_gimbal_device");
            break;
        case MAVLINK_MSG_ID_GIMBAL_MANAGER_SET_PITCHYAW:
            strcpy(mav_name, "gimbal_manager_set_pitchyaw");
            break;
        case MAVLINK_MSG_ID_GIMBAL_MANAGER_SET_MANUAL_CONTROL:
            strcpy(mav_name, "gimbal_manager_set_manual_control");
            break;
        case MAVLINK_MSG_ID_ESC_INFO:
            strcpy(mav_name, "esc_info");
            break;
        case MAVLINK_MSG_ID_ESC_STATUS:
            strcpy(mav_name, "esc_status");
            break;
        case MAVLINK_MSG_ID_WIFI_CONFIG_AP:
            strcpy(mav_name, "wifi_config_ap");
            break;
        case MAVLINK_MSG_ID_AIS_VESSEL:
            strcpy(mav_name, "ais_vessel");
            break;
        case MAVLINK_MSG_ID_UAVCAN_NODE_STATUS:
            strcpy(mav_name, "uavcan_node_status");
            break;
        case MAVLINK_MSG_ID_UAVCAN_NODE_INFO:
            strcpy(mav_name, "uavcan_node_info");
            break;
        case MAVLINK_MSG_ID_PARAM_EXT_REQUEST_READ:
            strcpy(mav_name, "param_ext_request_read");
            break;
        case MAVLINK_MSG_ID_PARAM_EXT_REQUEST_LIST:
            strcpy(mav_name, "param_ext_request_list");
            break;
        case MAVLINK_MSG_ID_PARAM_EXT_VALUE:
            strcpy(mav_name, "param_ext_value");
            break;
        case MAVLINK_MSG_ID_PARAM_EXT_SET:
            strcpy(mav_name, "param_ext_set");
            break;
        case MAVLINK_MSG_ID_PARAM_EXT_ACK:
            strcpy(mav_name, "param_ext_ack");
            break;
        case MAVLINK_MSG_ID_OBSTACLE_DISTANCE:
            strcpy(mav_name, "obstacle_distance");
            break;
        case MAVLINK_MSG_ID_ODOMETRY:
            strcpy(mav_name, "odometry");
            break;
        case MAVLINK_MSG_ID_TRAJECTORY_REPRESENTATION_WAYPOINTS:
            strcpy(mav_name, "trajectory_representation_waypoints");
            break;
        case MAVLINK_MSG_ID_TRAJECTORY_REPRESENTATION_BEZIER:
            strcpy(mav_name, "trajectory_representation_bezier");
            break;
        case MAVLINK_MSG_ID_CELLULAR_STATUS:
            strcpy(mav_name, "cellular_status");
            break;
        case MAVLINK_MSG_ID_ISBD_LINK_STATUS:
            strcpy(mav_name, "isbd_link_status");
            break;
        case MAVLINK_MSG_ID_CELLULAR_CONFIG:
            strcpy(mav_name, "cellular_config");
            break;
        case MAVLINK_MSG_ID_RAW_RPM:
            strcpy(mav_name, "raw_rpm");
            break;
        case MAVLINK_MSG_ID_UTM_GLOBAL_POSITION:
            strcpy(mav_name, "utm_global_position");
            break;
        case MAVLINK_MSG_ID_DEBUG_FLOAT_ARRAY:
            strcpy(mav_name, "debug_float_array");
            break;
        case MAVLINK_MSG_ID_ORBIT_EXECUTION_STATUS:
            strcpy(mav_name, "orbit_execution_status");
            break;
        case MAVLINK_MSG_ID_SMART_BATTERY_INFO:
            strcpy(mav_name, "smart_battery_info");
            break;
        case MAVLINK_MSG_ID_GENERATOR_STATUS:
            strcpy(mav_name, "generator_status");
            break;
        case MAVLINK_MSG_ID_ACTUATOR_OUTPUT_STATUS:
            strcpy(mav_name, "actuator_output_status");
            break;
        case MAVLINK_MSG_ID_TIME_ESTIMATE_TO_TARGET:
            strcpy(mav_name, "time_estimate_to_target");
            break;
        case MAVLINK_MSG_ID_TUNNEL:
            strcpy(mav_name, "tunnel");
            break;
        case MAVLINK_MSG_ID_CAN_FRAME:
            strcpy(mav_name, "can_frame");
            break;
        case MAVLINK_MSG_ID_ONBOARD_COMPUTER_STATUS:
            strcpy(mav_name, "onboard_computer_status");
            break;
        case MAVLINK_MSG_ID_COMPONENT_INFORMATION:
            strcpy(mav_name, "component_information");
            break;
        case MAVLINK_MSG_ID_COMPONENT_METADATA:
            strcpy(mav_name, "component_metadata");
            break;
        case MAVLINK_MSG_ID_PLAY_TUNE_V2:
            strcpy(mav_name, "play_tune_v2");
            break;
        case MAVLINK_MSG_ID_SUPPORTED_TUNES:
            strcpy(mav_name, "supported_tunes");
            break;
        case MAVLINK_MSG_ID_EVENT:
            strcpy(mav_name, "event");
            break;
        case MAVLINK_MSG_ID_CURRENT_EVENT_SEQUENCE:
            strcpy(mav_name, "current_event_sequence");
            break;
        case MAVLINK_MSG_ID_REQUEST_EVENT:
            strcpy(mav_name, "request_event");
            break;
        case MAVLINK_MSG_ID_RESPONSE_EVENT_ERROR:
            strcpy(mav_name, "response_event_error");
            break;
        case MAVLINK_MSG_ID_CANFD_FRAME:
            strcpy(mav_name, "canfd_frame");
            break;
        case MAVLINK_MSG_ID_CAN_FILTER_MODIFY:
            strcpy(mav_name, "can_filter_modify");
            break;
        case MAVLINK_MSG_ID_WHEEL_DISTANCE:
            strcpy(mav_name, "wheel_distance");
            break;
        case MAVLINK_MSG_ID_WINCH_STATUS:
            strcpy(mav_name, "winch_status");
            break;
        case MAVLINK_MSG_ID_OPEN_DRONE_ID_BASIC_ID:
            strcpy(mav_name, "open_drone_id_basic_id");
            break;
        case MAVLINK_MSG_ID_OPEN_DRONE_ID_LOCATION:
            strcpy(mav_name, "open_drone_id_location");
            break;
        case MAVLINK_MSG_ID_OPEN_DRONE_ID_AUTHENTICATION:
            strcpy(mav_name, "open_drone_id_authentication");
            break;
        case MAVLINK_MSG_ID_OPEN_DRONE_ID_SELF_ID:
            strcpy(mav_name, "open_drone_id_self_id");
            break;
        case MAVLINK_MSG_ID_OPEN_DRONE_ID_SYSTEM:
            strcpy(mav_name, "open_drone_id_system");
            break;
        case MAVLINK_MSG_ID_OPEN_DRONE_ID_OPERATOR_ID:
            strcpy(mav_name, "open_drone_id_operator_id");
            break;
        case MAVLINK_MSG_ID_OPEN_DRONE_ID_MESSAGE_PACK:
            strcpy(mav_name, "open_drone_id_message_pack");
            break;
        case MAVLINK_MSG_ID_HYGROMETER_SENSOR:
            strcpy(mav_name, "hygrometer_sensor");
            break;

        default:
            break;
    }

    return;
}


#endif
