/**
 * Extrinsic Configuration File
 * This file is used by voxl-vision-px4, voxl-qvio-server, and voxl-vins-server.
 *
 * This configuration file serves to describe the static relations (translation
 * and rotation) between sensors and bodies on a drone. Mostly importantly it
 * configures the camera-IMU extrinsic relation for use by VIO. However, the
 * user may expand this file to store many more relations if they wish. By
 * consolidating these relations in one file, multiple processes that need this
 * data can all be configured by this one configuration file. Also, copies of
 * this file may be saved which describe particular drone platforms. The
 * defaults describe the VOXL M500 drone reference platform.
 *
 * The file is constructed as an array of multiple extrinsic entries, each
 * describing the relation from one parent to one child. Nothing stops you from
 * having duplicates but this is not advised.
 *
 * The rotation is stored in the file as a Tait-Bryan rotation sequence in
 * intrinsic XYZ order in units of degrees. This corresponds to the parent
 * rolling about its X axis, followed by pitching about its new Y axis, and
 * finally yawing around its new Z axis to end up aligned with the child
 * coordinate frame.
 *
 * The helper read function will read out and populate the associated data
 * struct in both Tait-Bryan and rotation matrix format so the calling process
 * can use either. Helper functions are provided to convert back and forth
 * between the two rotation formats.
 *
 * Note that we elect to use the intrinsic XYZ rotation in units of degrees for
 * ease of use when doing camera-IMU extrinsic relations in the field. This is
 * not the same order as the aerospace yaw-pitch-roll (ZYX) sequence as used by
 * the rc_math library. However, since the camera Z axis points out the lens, it
 * is helpful for the last step in the rotation sequence to rotate the camera
 * about its lens after first rotating the IMU's coordinate frame to point in
 * the right direction by Roll and Pitch.
 *
 * The following online rotation calculator is useful for experimenting with
 * rotation sequences: https://www.andre-gaschler.com/rotationconverter/
 *
 * The Translation vector should represent the center of the child coordinate
 * frame with respect to the parent coordinate frame in units of meters.
 *
 * The parent and child name strings should not be longer than 63 characters.
 *
 * The relation from Body to Ground is a special case where only the Z value is
 * read by voxl-vision-px4 and voxl-qvio-server so that these services know the
 * height of the drone's center of mass (and tracking camera) above the ground
 * when the drone is sitting on its landing gear ready for takeoff.
 *
 **/
{
	"name":	"seeker_v1_voxlcam",
	"extrinsics":	[{
			"parent":	"imu0",
			"child":	"tracking",
			"T_child_wrt_parent":	[0.014, 0.005, 0.005],
			"RPY_parent_to_child":	[-45, 0, 0]
		}, {
			"parent":	"imu1",
			"child":	"tracking",
			"T_child_wrt_parent":	[-0.0344, 0.03, 0.005],
			"RPY_parent_to_child":	[-45, 0, 0]
		}, {
			"parent":	"body",
			"child":	"imu0",
			"T_child_wrt_parent":	[0.0496, -0.0152, 0.0161],
			"RPY_parent_to_child":	[0, 90, 90]
		}, {
			"parent":	"body",
			"child":	"imu1",
			"T_child_wrt_parent":	[0.0465, 0.0332, -0.013],
			"RPY_parent_to_child":	[0, 90, 90]
		}, {
			"parent":	"body",
			"child":	"stereo_l",
			"T_child_wrt_parent":	[0.055, -0.04, 0],
			"RPY_parent_to_child":	[0, 90, 90]
		}, {
			"parent":	"body",
			"child":	"ground",
			"T_child_wrt_parent":	[0, 0, 0.045],
			"RPY_parent_to_child":	[0, 0, 0]
		}, {
			"parent":	"imu1",
			"child":	"imu0",
			"T_child_wrt_parent":	[-0.0484, 0.037, 0.002],
			"RPY_parent_to_child":	[0, 0, 0]
		}, {
			"parent":	"body",
			"child":	"tof",
			"T_child_wrt_parent":	[0.055, 0, 0],
			"RPY_parent_to_child":	[0, 90, -90]
		}]
}
